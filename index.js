const express = require('express')
const bodyParser = require("body-parser");
const app = express()

const users = require('./data/users.json')

app.set("view engine", "ejs");
app.use(express.static("public"));

app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.render("home/index")
})

app.listen(3000,()=>{
    console.log("listeng");
})

app. post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  const user = users.find((user) => {
    return user.username === username;
  });

  

  if (user) {
    const passwordMatched = user.password === password;

    if (passwordMatched){
      res.send(user);
    } else {
      res.status(404).send("password salah");
    }
  } else{
     res.status(401).send("user tidak ada");
  }
});

app.get("/users", (req, res) => {
  res.send(users);
});